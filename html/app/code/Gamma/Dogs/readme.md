# Dogs Challenge

## Instructions:
1. Fork this repo and clone into your machine
2. Copy over the Dogs module to your magento installation (Or create a new one if you wish)
3. Complete the challenge
4. Place your changes in this repo.
5. Create a pull request from your fork to the base repo.

## Description:
A controller with a view model has already been created. You can access the route via _/dogs/breeds/index_

You need to add functionality to this view so the user can do the following:

- View a list of breeds
- Select a breed and view information about the breed:
    - Image
    - Sub-breeds
    - Books about that breed
    
Try to fulfil as many of the requirements as you can!
    
### Formal Requirements

1. User will be able to view a list of breeds
2. User will be able to select a specific breed
3. User will be able to view sub-breeds of a breed
4. User will be able to view books about the breed they select
5. User will be able to select a book, view its cover, reviews and get an option to read or download it (if applicable)


### Non-formal Requirements

1. It has to look pretty
2. Has to be usable. You can use inline javascript if you need.

### Resources
You will have these 3 APIs at your disposal. You may not need all of them:
- https://dog.ceo/dog-api/
- https://openlibrary.org/developers/api
- https://www.goodreads.com/api

If you want a hacky way to use jQuery inline (You will learn the proper way later - Never do this in a real place!)

    setTimeout(() => {
        jQuery(function() {
            // Your jQuery code here
        });
       
    }, 500);

## Bonus:
In the module ITP, a new homework file _RotateImage_ was included. If you finish the above tasks early, or need a break,
complete that file so that _magento itp:homework:test_ successfully passes. Once it does, write comments on your code explaining how and why it works.

Finally, if my dad is able to use your breeds page to find books he wants to read, you will get a big bonus - So get your UX game going!

## Evaluation Criteria:
| Item                       | Description                                                                                                                     | Value |
|----------------------------|---------------------------------------------------------------------------------------------------------------------------------|-------|
| List and select Breeds     | The user can view a list of breeds and select one                                                                               | 10%   |
| View sub-breeds and books  | The user can view the sub-breeds of a breed  and books about the breed                                                          | 15%   |
| View basic book info       | The user can select a book and view basic information about it (title, cover, ISBN)                                             | 20%   |
| View book reviews and read | The user can view book reviews                                                                                                  | 15%   |
| Download/Read book         | The user can download or read books (if the book allows it)                                                                     | 5%    |
| SOLID Code                 | Your code is well-written according to solid principles                                                                         | 15%   |
| Finish in time             | Commit your changes before the deadline. (If you commit before the end of the day, you get half the value, 0% for the next day) | 20%   |
| Bonus: Rotate Image        | Complete - and explain - the rotate image code.                                                                                 | 10%   |
| Bonus: Usable by a dad     | Your breeds app is good enough that my dad can use it effectively.                                                              | 20%   |