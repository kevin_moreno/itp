<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 23/04/19
 * Time: 01:18 PM
 */

namespace Gamma\PokeAPI\Api;


use Gamma\PokeAPI\Api\Data\PokemonInterface;

interface PokedexInterface
{
    public function getPokemon(string $name): PokemonInterface;
}